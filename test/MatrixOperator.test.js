import {
  assertEquals,
  assertStrictEquals,
} from 'https://deno.land/std@0.95.0/testing/asserts.ts';

import MatrixOperator from '../src/MatrixOperator.js';

Deno.test({
  name: 'new matrix',
  fn: () => {
    const expected = [[], [], []];
    const actual = MatrixOperator.makeNewMatrix();

    assertEquals(actual, expected);
    assertStrictEquals(actual.length, expected.length);

    for (const row in actual)
      assertStrictEquals(actual[row].length, expected[row].length);
  },
});

const sumTestFixtures = [
  {
    expected: [
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9],
    ],
    actual: MatrixOperator.sum(
      [
        [7, 1, 7],
        [17, 0, 17],
        [29, 57, 29],
      ],
      [
        [-6, 1, -4],
        [-13, 5, -11],
        [-22, -49, -20],
      ]
    ),
  },
  {
    expected: [
      [1, 0, 1],
      [0, 1, 0],
      [1, 0, 1],
    ],
    actual: MatrixOperator.sum(
      [
        [1, 2, 3],
        [5, 8, 13],
        [21, 34, 55],
      ],
      [
        [0, -2, -2],
        [-5, -7, -13],
        [-20, -34, -54],
      ]
    ),
  },
];

for (const { expected, actual } of sumTestFixtures)
  Deno.test({
    name: 'matrix sum',
    fn: () => {
      for (const row in actual)
        for (const element in actual[row])
          assertStrictEquals(actual[row][element], expected[row][element]);
    },
  });

const subtractionTestFixtures = [
  {
    expected: [
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9],
    ],
    actual: MatrixOperator.subtract(
      [
        [7, 1, 7],
        [17, 0, 17],
        [29, 57, 29],
      ],
      [
        [6, -1, 4],
        [13, -5, 11],
        [22, 49, 20],
      ]
    ),
  },
  {
    expected: [
      [1, 0, 1],
      [0, 1, 0],
      [1, 0, 1],
    ],
    actual: MatrixOperator.subtract(
      [
        [1, 2, 3],
        [5, 8, 13],
        [21, 34, 55],
      ],
      [
        [0, 2, 2],
        [5, 7, 13],
        [20, 34, 54],
      ]
    ),
  },
];

for (const { expected, actual } of subtractionTestFixtures)
  Deno.test({
    name: 'matrix subtraction',
    fn: () => {
      for (const row in actual)
        for (const element in actual[row])
          assertStrictEquals(actual[row][element], expected[row][element]);
    },
  });

const multiplicationTestFixtures = [
  {
    expected: [
      [3, 4, 5],
      [6, 8, 10],
      [3, 4, 5],
    ],
    actual: MatrixOperator.multiply(
      [
        [0, 1, 0],
        [1, 0, 1],
        [0, 1, 0],
      ],
      [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
      ]
    ),
  },
];

for (const { expected, actual } of multiplicationTestFixtures)
  Deno.test({
    name: 'matrix multiplication',
    fn: () => {
      for (const row in actual)
        for (const element in actual[row])
          assertStrictEquals(actual[row][element], expected[row][element]);
    },
  });

const determinantTestFixtures = [
  {
    expected: 49,
    actual: MatrixOperator.calculateDeterminant([
      [2, -3, 1],
      [2, 0, -1],
      [1, 4, 5],
    ]),
  },
  {
    expected: 324,
    actual: MatrixOperator.calculateDeterminant([
      [-7, -10, 4],
      [3, -9, 2],
      [7, 1, 2],
    ]),
  },
];

for (const { expected, actual } of determinantTestFixtures)
  Deno.test({
    name: 'matrix determinant',
    fn: () => {
      for (const row in actual)
        for (const element in actual[row])
          assertStrictEquals(actual[row][element], expected[row][element]);
    },
  });

const transposeTestFixtures = [
  {
    expected: [
      [2, 2, 1],
      [-3, 0, 4],
      [1, -1, 5],
    ],
    actual: MatrixOperator.transpose([
      [2, -3, 1],
      [2, 0, -1],
      [1, 4, 5],
    ]),
  },
  {
    expected: [
      [-7, -10, 4],
      [3, -9, 2],
      [7, 1, 2],
    ],
    actual: MatrixOperator.transpose([
      [-7, 3, 7],
      [-10, -9, 1],
      [4, 2, 2],
    ]),
  },
];

for (const { expected, actual } of transposeTestFixtures)
  Deno.test({
    name: 'matrix transpose',
    fn: () => {
      for (const row in actual)
        for (const element in actual[row])
          assertStrictEquals(actual[row][element], expected[row][element]);
    },
  });

const adjugateTestFixtures = [
  {
    expected: [
      [-8, 18, -4],
      [-5, 12, -1],
      [4, -6, 2],
    ],
    actual: MatrixOperator.adjugate([
      [-3, 2, -5],
      [-1, 0, -2],
      [3, -4, 1],
    ]),
  },
  {
    expected: [
      [4, 6, 7],
      [1, 0, 1],
      [2, 3, 2],
    ],
    actual: MatrixOperator.adjugate([
      [-1, 3, 2],
      [0, -2, 1],
      [1, 0, -2],
    ]),
  },
];

for (const { expected, actual } of adjugateTestFixtures)
  Deno.test({
    name: 'matrix adjugate',
    fn: () => {
      for (const row in actual)
        for (const element in actual[row])
          assertStrictEquals(actual[row][element], expected[row][element]);
    },
  });

const invertTestFixtures = [
  {
    expected: [
      [12 / 11, -6 / 11, -1 / 11],
      [5 / 22, 3 / 22, -5 / 22],
      [-2 / 11, 1 / 11, 2 / 11],
    ],
    actual: MatrixOperator.invert([
      [1, 2, 3],
      [0, 4, 5],
      [1, 0, 6],
    ]),
  },
  {
    expected: [
      [-1 / 5, 3 / 5, -1 / 5],
      [4 / 5, -2 / 5, -1 / 5],
      [-2 / 5, 1 / 5, 3 / 5],
    ],
    actual: MatrixOperator.invert([
      [1, 2, 1],
      [2, 1, 1],
      [0, 1, 2],
    ]),
  },
];

for (const { expected, actual } of invertTestFixtures)
  Deno.test({
    name: 'matrix invert',
    fn: () => {
      for (const row in actual)
        for (const element in actual[row])
          assertStrictEquals(
            Math.floor(actual[row][element] * 10 ** 5) / 10 ** 5,
            Math.floor(expected[row][element] * 10 ** 5) / 10 ** 5
          );
    },
  });
