import { assertStrictEquals } from 'https://deno.land/std@0.95.0/testing/asserts.ts';

import ComplexMatrixOperator from '../src/ComplexMatrixOperator.js';
import ComplexNumber from '../src/ComplexNumber.js';

const sumTestFixtures = [
  {
    expected: [
      [
        new ComplexNumber({ real: 10, imaginary: 10 }),
        new ComplexNumber({ real: 10, imaginary: 10 }),
        new ComplexNumber({ real: 10, imaginary: 10 }),
      ],
      [
        new ComplexNumber({ real: -10, imaginary: -10 }),
        new ComplexNumber({ real: -10, imaginary: -10 }),
        new ComplexNumber({ real: -10, imaginary: -10 }),
      ],
      [
        new ComplexNumber({ real: 10, imaginary: 10 }),
        new ComplexNumber({ real: 10, imaginary: 10 }),
        new ComplexNumber({ real: 10, imaginary: 10 }),
      ],
    ],
    actual: ComplexMatrixOperator.sum(
      [
        [
          new ComplexNumber({ real: 3, imaginary: 2 }),
          new ComplexNumber({ real: 5, imaginary: 8 }),
          new ComplexNumber({ imaginary: 1 }),
        ],
        [
          new ComplexNumber({ real: -3, imaginary: -2 }),
          new ComplexNumber({ real: -7 }),
          new ComplexNumber({ real: 3, imaginary: 2 }),
        ],
        [
          new ComplexNumber({ real: 7 }),
          new ComplexNumber({ real: 5, imaginary: 11 }),
          new ComplexNumber({ real: 3, imaginary: 2 }),
        ],
      ],
      [
        [
          new ComplexNumber({ real: 7, imaginary: 8 }),
          new ComplexNumber({ real: 5, imaginary: 2 }),
          new ComplexNumber({ real: 10, imaginary: 9 }),
        ],
        [
          new ComplexNumber({ real: -7, imaginary: -8 }),
          new ComplexNumber({ real: -3, imaginary: -10 }),
          new ComplexNumber({ real: -13, imaginary: -12 }),
        ],
        [
          new ComplexNumber({ real: 3, imaginary: 10 }),
          new ComplexNumber({ real: 5, imaginary: -1 }),
          new ComplexNumber({ real: 7, imaginary: 8 }),
        ],
      ]
    ),
  },
  {
    expected: [
      [
        new ComplexNumber({ real: 9, imaginary: 1 }),
        new ComplexNumber({ real: 8, imaginary: 2 }),
        new ComplexNumber({ real: 7, imaginary: 3 }),
      ],
      [
        new ComplexNumber({ real: 6, imaginary: 4 }),
        new ComplexNumber({ real: 5, imaginary: 5 }),
        new ComplexNumber({ real: 4, imaginary: 6 }),
      ],
      [
        new ComplexNumber({ real: 3, imaginary: 7 }),
        new ComplexNumber({ real: 2, imaginary: 8 }),
        new ComplexNumber({ real: 1, imaginary: 9 }),
      ],
    ],
    actual: ComplexMatrixOperator.sum(
      [
        [
          new ComplexNumber({ imaginary: 1 }),
          new ComplexNumber({ real: 1, imaginary: 2 }),
          new ComplexNumber({ imaginary: 5 }),
        ],
        [
          new ComplexNumber({ imaginary: 13 }),
          new ComplexNumber({ real: 21 }),
          new ComplexNumber({ real: 55, imaginary: 89 }),
        ],
        [
          new ComplexNumber({ real: 144 }),
          new ComplexNumber({ real: 377, imaginary: 610 }),
          new ComplexNumber({ imaginary: 1597 }),
        ],
      ],
      [
        [
          new ComplexNumber({ real: 9 }),
          new ComplexNumber({ real: 7 }),
          new ComplexNumber({ real: 7, imaginary: -2 }),
        ],
        [
          new ComplexNumber({ real: 6, imaginary: -9 }),
          new ComplexNumber({ real: -16, imaginary: 5 }),
          new ComplexNumber({ real: -51, imaginary: -83 }),
        ],
        [
          new ComplexNumber({ real: -141, imaginary: 7 }),
          new ComplexNumber({ real: -375, imaginary: -602 }),
          new ComplexNumber({ real: 1, imaginary: -1588 }),
        ],
      ]
    ),
  },
];

for (const { expected, actual } of sumTestFixtures)
  Deno.test({
    name: 'matrix complex sum',
    fn: () => {
      for (const row in actual)
        for (const element in actual[row]) {
          assertStrictEquals(
            actual[row][element].real,
            expected[row][element].real
          );
          assertStrictEquals(
            actual[row][element].imaginary,
            expected[row][element].imaginary
          );
        }
    },
  });

const subtractionTestFixtures = [
  {
    expected: [
      [
        new ComplexNumber({ real: 7, imaginary: 8 }),
        new ComplexNumber({ real: 5, imaginary: 2 }),
        new ComplexNumber({ real: 10, imaginary: 9 }),
      ],
      [
        new ComplexNumber({ real: -7, imaginary: -8 }),
        new ComplexNumber({ real: -3, imaginary: -10 }),
        new ComplexNumber({ real: -13, imaginary: -12 }),
      ],
      [
        new ComplexNumber({ real: 3, imaginary: 10 }),
        new ComplexNumber({ real: 5, imaginary: -1 }),
        new ComplexNumber({ real: 7, imaginary: 8 }),
      ],
    ],
    actual: ComplexMatrixOperator.subtract(
      [
        [
          new ComplexNumber({ real: 10, imaginary: 10 }),
          new ComplexNumber({ real: 10, imaginary: 10 }),
          new ComplexNumber({ real: 10, imaginary: 10 }),
        ],
        [
          new ComplexNumber({ real: -10, imaginary: -10 }),
          new ComplexNumber({ real: -10, imaginary: -10 }),
          new ComplexNumber({ real: -10, imaginary: -10 }),
        ],
        [
          new ComplexNumber({ real: 10, imaginary: 10 }),
          new ComplexNumber({ real: 10, imaginary: 10 }),
          new ComplexNumber({ real: 10, imaginary: 10 }),
        ],
      ],
      [
        [
          new ComplexNumber({ real: 3, imaginary: 2 }),
          new ComplexNumber({ real: 5, imaginary: 8 }),
          new ComplexNumber({ imaginary: 1 }),
        ],
        [
          new ComplexNumber({ real: -3, imaginary: -2 }),
          new ComplexNumber({ real: -7 }),
          new ComplexNumber({ real: 3, imaginary: 2 }),
        ],
        [
          new ComplexNumber({ real: 7 }),
          new ComplexNumber({ real: 5, imaginary: 11 }),
          new ComplexNumber({ real: 3, imaginary: 2 }),
        ],
      ]
    ),
  },
  {
    expected: [
      [
        new ComplexNumber({ real: 9 }),
        new ComplexNumber({ real: 7 }),
        new ComplexNumber({ real: 7, imaginary: -2 }),
      ],
      [
        new ComplexNumber({ real: 6, imaginary: -9 }),
        new ComplexNumber({ real: -16, imaginary: 5 }),
        new ComplexNumber({ real: -51, imaginary: -83 }),
      ],
      [
        new ComplexNumber({ real: -141, imaginary: 7 }),
        new ComplexNumber({ real: -375, imaginary: -602 }),
        new ComplexNumber({ real: 1, imaginary: -1588 }),
      ],
    ],
    actual: ComplexMatrixOperator.subtract(
      [
        [
          new ComplexNumber({ real: 9, imaginary: 1 }),
          new ComplexNumber({ real: 8, imaginary: 2 }),
          new ComplexNumber({ real: 7, imaginary: 3 }),
        ],
        [
          new ComplexNumber({ real: 6, imaginary: 4 }),
          new ComplexNumber({ real: 5, imaginary: 5 }),
          new ComplexNumber({ real: 4, imaginary: 6 }),
        ],
        [
          new ComplexNumber({ real: 3, imaginary: 7 }),
          new ComplexNumber({ real: 2, imaginary: 8 }),
          new ComplexNumber({ real: 1, imaginary: 9 }),
        ],
      ],
      [
        [
          new ComplexNumber({ imaginary: 1 }),
          new ComplexNumber({ real: 1, imaginary: 2 }),
          new ComplexNumber({ imaginary: 5 }),
        ],
        [
          new ComplexNumber({ imaginary: 13 }),
          new ComplexNumber({ real: 21 }),
          new ComplexNumber({ real: 55, imaginary: 89 }),
        ],
        [
          new ComplexNumber({ real: 144 }),
          new ComplexNumber({ real: 377, imaginary: 610 }),
          new ComplexNumber({ imaginary: 1597 }),
        ],
      ]
    ),
  },
];

for (const { expected, actual } of subtractionTestFixtures)
  Deno.test({
    name: 'matrix complex subtraction',
    fn: () => {
      for (const row in actual)
        for (const element in actual[row]) {
          assertStrictEquals(
            actual[row][element].real,
            expected[row][element].real
          );
          assertStrictEquals(
            actual[row][element].imaginary,
            expected[row][element].imaginary
          );
        }
    },
  });

const multiplicationTestFixtures = [
  {
    expected: [
      [
        new ComplexNumber({ real: 16, imaginary: 29 }),
        new ComplexNumber({ real: 11, imaginary: 24 }),
        new ComplexNumber({ real: 3, imaginary: 26 }),
      ],
      [
        new ComplexNumber({ real: 17, imaginary: 37 }),
        new ComplexNumber({ real: -21, imaginary: 55 }),
        new ComplexNumber({ real: 7, imaginary: 21 }),
      ],
      [
        new ComplexNumber({ real: 61, imaginary: 3 }),
        new ComplexNumber({ real: 97, imaginary: 61 }),
        new ComplexNumber({ real: -2, imaginary: 36 }),
      ],
    ],
    actual: ComplexMatrixOperator.multiply(
      [
        [
          new ComplexNumber({ real: 1, imaginary: 2 }),
          new ComplexNumber({ real: 2, imaginary: 1 }),
          new ComplexNumber({ real: 3, imaginary: 4 }),
        ],
        [
          new ComplexNumber({ real: 3, imaginary: 7 }),
          new ComplexNumber({ real: 2 }),
          new ComplexNumber({ real: 4, imaginary: 2 }),
        ],
        [
          new ComplexNumber({ real: 7 }),
          new ComplexNumber({ imaginary: 6 }),
          new ComplexNumber({ real: 4, imaginary: 1 }),
        ],
      ],
      [
        [
          new ComplexNumber({ real: 3, imaginary: 2 }),
          new ComplexNumber({ real: 5, imaginary: 8 }),
          new ComplexNumber({ imaginary: 1 }),
        ],
        [
          new ComplexNumber({ real: -3, imaginary: -2 }),
          new ComplexNumber({ imaginary: -7 }),
          new ComplexNumber({ real: 3, imaginary: 2 }),
        ],
        [
          new ComplexNumber({ real: 7 }),
          new ComplexNumber({ real: 5 }),
          new ComplexNumber({ real: 3, imaginary: 2 }),
        ],
      ]
    ),
  },
];

for (const { expected, actual } of multiplicationTestFixtures)
  Deno.test({
    name: 'matrix complex multiplication',
    fn: () => {
      for (const row in actual)
        for (const element in actual[row]) {
          assertStrictEquals(
            actual[row][element].real,
            expected[row][element].real
          );
          assertStrictEquals(
            actual[row][element].imaginary,
            expected[row][element].imaginary
          );
        }
    },
  });

const determinantTestFixtures = [
  {
    expected: new ComplexNumber({ real: -515, imaginary: -43 }),
    actual: ComplexMatrixOperator.calculateDeterminant([
      [
        new ComplexNumber({ real: 1, imaginary: 2 }),
        new ComplexNumber({ real: -4, imaginary: -3 }),
        new ComplexNumber({ real: 6, imaginary: 5 }),
      ],
      [
        new ComplexNumber({ real: -3, imaginary: 4 }),
        new ComplexNumber({ real: 2, imaginary: 3 }),
        new ComplexNumber({ real: 5, imaginary: -6 }),
      ],
      [
        new ComplexNumber({ real: 5, imaginary: 6 }),
        new ComplexNumber({ real: 6, imaginary: -6 }),
        new ComplexNumber({ real: 3, imaginary: 5 }),
      ],
    ]),
  },
  {
    expected: new ComplexNumber({ real: 307, imaginary: 352 }),
    actual: ComplexMatrixOperator.calculateDeterminant([
      [
        new ComplexNumber({ imaginary: 1 }),
        new ComplexNumber({ real: 2 }),
        new ComplexNumber({ imaginary: 13 }),
      ],
      [
        new ComplexNumber({ real: 1, imaginary: 2 }),
        new ComplexNumber({ real: 5, imaginary: 7 }),
        new ComplexNumber({ imaginary: 2 }),
      ],
      [
        new ComplexNumber({ real: 3, imaginary: 5 }),
        new ComplexNumber({ real: 11 }),
        new ComplexNumber({ imaginary: 7 }),
      ],
    ]),
  },
];

for (const { expected, actual } of determinantTestFixtures)
  Deno.test({
    name: 'matrix complex determinant',
    fn: () => {
      for (const row in actual)
        for (const element in actual[row]) {
          assertStrictEquals(
            actual[row][element].real,
            expected[row][element].real
          );
          assertStrictEquals(
            actual[row][element].imaginary,
            expected[row][element].imaginary
          );
        }
    },
  });

const adjugateTestFixtures = [
  {
    expected: [
      [
        new ComplexNumber({ real: -3, imaginary: 85 }),
        new ComplexNumber({ real: 40, imaginary: 86 }),
        new ComplexNumber({ real: -35, imaginary: -19 }),
      ],
      [
        new ComplexNumber({ real: 87, imaginary: 93 }),
        new ComplexNumber({ real: -7, imaginary: -50 }),
        new ComplexNumber({ real: -60, imaginary: -50 }),
      ],
      [
        new ComplexNumber({ real: 14, imaginary: 15 }),
        new ComplexNumber({ real: 25, imaginary: -65 }),
        new ComplexNumber({ real: -28, imaginary: 14 }),
      ],
    ],
    actual: ComplexMatrixOperator.adjugate([
      [
        new ComplexNumber({ real: 1, imaginary: 2 }),
        new ComplexNumber({ real: -4, imaginary: -3 }),
        new ComplexNumber({ real: 6, imaginary: 5 }),
      ],
      [
        new ComplexNumber({ real: -3, imaginary: 4 }),
        new ComplexNumber({ real: 2, imaginary: 3 }),
        new ComplexNumber({ real: 5, imaginary: -6 }),
      ],
      [
        new ComplexNumber({ real: 5, imaginary: 6 }),
        new ComplexNumber({ real: 6, imaginary: -6 }),
        new ComplexNumber({ real: 3, imaginary: 5 }),
      ],
    ]),
  },
  {
    expected: [
      [
        new ComplexNumber({ real: -1, imaginary: -31 }),
        new ComplexNumber({ real: 30, imaginary: -28 }),
        new ComplexNumber({ real: -8, imaginary: 34 }),
      ],
      [
        new ComplexNumber({ real: 0, imaginary: 52 }),
        new ComplexNumber({ real: 5, imaginary: 5 }),
        new ComplexNumber({ real: 12, imaginary: -18 }),
      ],
      [
        new ComplexNumber({ real: -15, imaginary: 39 }),
        new ComplexNumber({ real: -26, imaginary: 66 }),
        new ComplexNumber({ real: 13, imaginary: 13 }),
      ],
    ],
    actual: ComplexMatrixOperator.adjugate([
      [
        new ComplexNumber({ real: 3, imaginary: 2 }),
        new ComplexNumber({ real: 5, imaginary: 8 }),
        new ComplexNumber({ imaginary: 1 }),
      ],
      [
        new ComplexNumber({ real: -3, imaginary: -2 }),
        new ComplexNumber({ imaginary: -7 }),
        new ComplexNumber({ real: 3, imaginary: 2 }),
      ],
      [
        new ComplexNumber({ real: 7 }),
        new ComplexNumber({ real: 5 }),
        new ComplexNumber({ real: 3, imaginary: 2 }),
      ],
    ]),
  },
];

for (const { expected, actual } of adjugateTestFixtures)
  Deno.test({
    name: 'complex matrix adjugate',
    fn: () => {
      for (const row in actual)
        for (const element in actual[row]) {
          assertStrictEquals(
            actual[row][element].real,
            expected[row][element].real
          );
          assertStrictEquals(
            actual[row][element].imaginary,
            expected[row][element].imaginary
          );
        }
    },
  });

const invertTestFixtures = [
  {
    expected: [
      [
        new ComplexNumber({
          real: -0.12665317626653175,
          imaginary: 0.036604755366047556,
        }),
        new ComplexNumber({
          real: -0.1467803714678037,
          imaginary: -0.0942400809424008,
        }),
        new ComplexNumber({
          real: 0.1484425814844258,
          imaginary: -0.002673990026739896,
        }),
      ],
      [
        new ComplexNumber({
          real: 0.21420828214208282,
          imaginary: -0.054491580544915805,
        }),
        new ComplexNumber({
          real: 0.01535737515357375,
          imaginary: -0.025836525258365252,
        }),
        new ComplexNumber({
          real: -0.08672400086724001,
          imaginary: -0.030570210305702106,
        }),
      ],
      [
        new ComplexNumber({
          real: 0.17637493676374938,
          imaginary: 0.020922165209221653,
        }),
        new ComplexNumber({
          real: 0.2991255329912553,
          imaginary: 0.03794175037941751,
        }),
        new ComplexNumber({
          real: 0.03992917539929175,
          imaginary: -0.06717496567174966,
        }),
      ],
    ],
    actual: ComplexMatrixOperator.invert([
      [
        new ComplexNumber({ real: 3, imaginary: 2 }),
        new ComplexNumber({ real: 5, imaginary: 8 }),
        new ComplexNumber({ imaginary: 1 }),
      ],
      [
        new ComplexNumber({ real: -3, imaginary: -2 }),
        new ComplexNumber({ imaginary: -7 }),
        new ComplexNumber({ real: 3, imaginary: 2 }),
      ],
      [
        new ComplexNumber({ real: 7 }),
        new ComplexNumber({ real: 5 }),
        new ComplexNumber({ real: 3, imaginary: 2 }),
      ],
    ]),
  },
  {
    expected: [
      [
        new ComplexNumber({
          real: -0.001880763478820855,
          imaginary: -0.07687166046798957,
        }),
        new ComplexNumber({
          real: -0.07013149175551188,
          imaginary: 0.018394148164774263,
        }),
        new ComplexNumber({
          real: 0.015561431626202552,
          imaginary: 0.02364716935411875,
        }),
      ],
      [
        new ComplexNumber({
          real: 0.0007139404525102928,
          imaginary: 0.057313792231585226,
        }),
        new ComplexNumber({
          real: -0.015645711108717014,
          imaginary: -0.030472152161196177,
        }),
        new ComplexNumber({
          real: -0.005597491413440977,
          imaginary: -0.011237821605590885,
        }),
      ],
      [
        new ComplexNumber({
          real: 0.006833363907878099,
          imaginary: 0.03306308992491632,
        }),
        new ComplexNumber({
          real: 0.07346282132845607,
          imaginary: -0.0063548511856187755,
        }),
        new ComplexNumber({
          real: -0.020622293758259136,
          imaginary: -0.020137005608609877,
        }),
      ],
    ],
    actual: ComplexMatrixOperator.invert([
      [
        new ComplexNumber({ real: 16, imaginary: 29 }),
        new ComplexNumber({ real: 11, imaginary: 24 }),
        new ComplexNumber({ real: 3, imaginary: 26 }),
      ],
      [
        new ComplexNumber({ real: 17, imaginary: 37 }),
        new ComplexNumber({ real: -21, imaginary: 55 }),
        new ComplexNumber({ real: 7, imaginary: 21 }),
      ],
      [
        new ComplexNumber({ real: 61, imaginary: 3 }),
        new ComplexNumber({ real: 97, imaginary: 61 }),
        new ComplexNumber({ real: -2, imaginary: 36 }),
      ],
    ]),
  },
];

for (const { expected, actual } of invertTestFixtures)
  Deno.test({
    name: 'complex matrix invert',
    fn: () => {
      for (const row in actual)
        for (const element in actual[row]) {
          assertStrictEquals(
            Math.floor(actual[row][element].real * 10 ** 5) / 10 ** 5,
            Math.floor(expected[row][element].real * 10 ** 5) / 10 ** 5
          );
          assertStrictEquals(
            Math.floor(actual[row][element].imaginary * 10 ** 5) / 10 ** 5,
            Math.floor(expected[row][element].imaginary * 10 ** 5) / 10 ** 5
          );
        }
    },
  });
