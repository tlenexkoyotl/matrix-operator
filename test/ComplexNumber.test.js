import { assertStrictEquals } from 'https://deno.land/std@0.95.0/testing/asserts.ts';

import ComplexNumber from '../src/ComplexNumber.js';

const toStringTestFixtures = [
  {
    expected: '0',
    actual: `${new ComplexNumber({ real: 0, imaginary: 0 })}`,
  },
  {
    expected: '3',
    actual: `${new ComplexNumber({ real: 3, imaginary: 0 })}`,
  },
  {
    expected: '2 + 7i',
    actual: `${new ComplexNumber({ real: 2, imaginary: 7 })}`,
  },
  {
    expected: '3 -8i',
    actual: `${new ComplexNumber({ real: 3, imaginary: -8 })}`,
  },
  {
    expected: '-23 -57i',
    actual: `${new ComplexNumber({ real: -23, imaginary: -57 })}`,
  },
  {
    expected: '5i',
    actual: `${new ComplexNumber({ real: 0, imaginary: 5 })}`,
  },
  {
    expected: '-13i',
    actual: `${new ComplexNumber({ real: 0, imaginary: -13 })}`,
  },
];

for (const { expected, actual } of toStringTestFixtures)
  Deno.test({
    name: 'complex number stringification',
    fn: () => {
      assertStrictEquals(actual, expected);
    },
  });

const sumTestFixtures = [
  {
    expected: new ComplexNumber({ real: 1, imaginary: 1 }),
    actual: new ComplexNumber({ real: 0, imaginary: 0 }).sum(
      new ComplexNumber({ real: 1, imaginary: 1 })
    ),
  },
  {
    expected: new ComplexNumber({ real: -1, imaginary: -3 }),
    actual: new ComplexNumber({ real: 7, imaginary: 7 }).sum(
      new ComplexNumber({ real: -8, imaginary: -10 })
    ),
  },
];

for (const { expected, actual } of sumTestFixtures)
  Deno.test({
    name: 'complex number sum',
    fn: () => {
      assertStrictEquals(actual.real, expected.real);
      assertStrictEquals(actual.imaginary, expected.imaginary);
    },
  });

const subtractTestFixtures = [
  {
    expected: new ComplexNumber({ real: 1, imaginary: 1 }),
    actual: new ComplexNumber({ real: 6, imaginary: 10 }).subtract(
      new ComplexNumber({ real: 5, imaginary: 9 })
    ),
  },
  {
    expected: new ComplexNumber({ real: 12, imaginary: 28 }),
    actual: new ComplexNumber({ real: 4, imaginary: 17 }).subtract(
      new ComplexNumber({ real: -8, imaginary: -11 })
    ),
  },
];

for (const { expected, actual } of subtractTestFixtures)
  Deno.test({
    name: 'complex number subtraction',
    fn: () => {
      assertStrictEquals(actual.real, expected.real);
      assertStrictEquals(actual.imaginary, expected.imaginary);
    },
  });

const multiplyTestFixtures = [
  {
    expected: new ComplexNumber({ real: 3, imaginary: 28 }),
    actual: new ComplexNumber({ real: 3, imaginary: 2 }).multiply(
      new ComplexNumber({ real: 5, imaginary: 6 })
    ),
  },
  {
    expected: new ComplexNumber({ real: 3, imaginary: 28 }),
    actual: new ComplexNumber({ real: 5, imaginary: 6 }).multiply(
      new ComplexNumber({ real: 3, imaginary: 2 })
    ),
  },
  {
    expected: new ComplexNumber({ real: 31, imaginary: 1 }),
    actual: new ComplexNumber({ real: 5, imaginary: -7 }).multiply(
      new ComplexNumber({ real: 2, imaginary: 3 })
    ),
  },
  {
    expected: new ComplexNumber({ real: 31, imaginary: 1 }),
    actual: new ComplexNumber({ real: 2, imaginary: 3 }).multiply(
      new ComplexNumber({ real: 5, imaginary: -7 })
    ),
  },
];

for (const { expected, actual } of multiplyTestFixtures)
  Deno.test({
    name: 'complex number multiplication',
    fn: () => {
      assertStrictEquals(actual.real, expected.real);
      assertStrictEquals(actual.imaginary, expected.imaginary);
    },
  });

const divideTestFixtures = [
  {
    expected: new ComplexNumber({ real: 27 / 61, imaginary: -8 / 61 }),
    actual: new ComplexNumber({ real: 3, imaginary: 2 }).divide(
      new ComplexNumber({ real: 5, imaginary: 6 })
    ),
  },
  {
    expected: new ComplexNumber({ real: 27 / 13, imaginary: 8 / 13 }),
    actual: new ComplexNumber({ real: 5, imaginary: 6 }).divide(
      new ComplexNumber({ real: 3, imaginary: 2 })
    ),
  },
  {
    expected: new ComplexNumber({ real: -11 / 13, imaginary: -29 / 13 }),
    actual: new ComplexNumber({ real: 5, imaginary: -7 }).divide(
      new ComplexNumber({ real: 2, imaginary: 3 })
    ),
  },
  {
    expected: new ComplexNumber({ real: -11 / 74, imaginary: 29 / 74 }),
    actual: new ComplexNumber({ real: 2, imaginary: 3 }).divide(
      new ComplexNumber({ real: 5, imaginary: -7 })
    ),
  },
];

for (const { expected, actual } of divideTestFixtures)
  Deno.test({
    name: 'complex number division',
    fn: () => {
      assertStrictEquals(actual.real, expected.real);
      assertStrictEquals(actual.imaginary, expected.imaginary);
    },
  });
