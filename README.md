# Matrix Operator

A set of simple 3x3 matrix calculator, including the sum, subtraction, multiplication and inversion operations.

Requires `deno` ≥ `1.9.2`.

## Commands:
### Run
    deno run main.js

### Test
    deno test

### Compile
    deno --unstable compile [--target <TARGET>] [--output <OUTPUT>] main.js

---
## License pending