import MatrixOperator from './MatrixOperator.js';
import ComplexNumber from './ComplexNumber.js';
import ComplexMatrixOperator from './ComplexMatrixOperator.js';

export default class UI {
  static main() {
    let runAgain = true;

    console.log(`\n\nBienvenido a la calculadora de matrices 😃`);

    while (runAgain) {
      const complex =
        prompt('Deseas operar con números complejos? y/n', 'y') === 'y';

      if (complex) this.execComplexMatrixMenu();
      else this.execMatrixMenu();

      const answer = prompt('\nDesea repetir? y/n', 'n') !== 'y';

      console.log('\n');

      if (answer) runAgain = false;
    }
  }

  static execMatrixMenu() {
    const instructions = `\nIntroduce un número para ejecutar una operación de matrices:
1 - suma
2 - resta
3 - multiplicación
4 - inversa\n`;
    const operation = parseInt(prompt(instructions, 1));

    switch (operation) {
      case 1:
        this.execMatrixSumCalculator();
        break;
      case 2:
        this.execMatrixSumCalculator();
        break;
      case 3:
        this.execMatrixMultiplicationCalculator();
        break;
      case 4:
        this.execComplexMatrixCalculator();
        break;
      default:
        break;
    }
  }

  static execComplexMatrixMenu() {
    const instructions = `\nIntroduce un número para ejecutar una operación de matrices complejas:
1 - suma
2 - resta
3 - multiplicación
4 - inversa\n`;
    const operation = parseInt(prompt(instructions, 1));

    switch (operation) {
      case 1:
        this.execComplexMatrixSumCalculator();
        break;
      case 2:
        this.execComplexMatrixSubtractionCalculator();
        break;
      case 3:
        this.execComplexMatrixMultiplicationCalculator();
        break;
      case 4:
        this.execInverseComplexMatrixCalculator();
        break;
      default:
        break;
    }
  }

  static inputFloat(message = 'Introduzca un valor', defaultValue = 0) {
    return parseFloat(prompt(message, defaultValue));
  }

  static inputMatrix(
    inputElement = (i, j) =>
      this.inputFloat(`Introduzca el valor real [${i}][${j}]: `, 0),
    message = 'Introduzca una matriz'
  ) {
    const matrix = ComplexMatrixOperator.makeNewMatrix();

    console.log(message);

    for (let i = 0; i < 3; i++)
      for (let j = 0; j < 3; j++) matrix[i][j] = inputElement(i, j);

    return matrix;
  }

  static inputComplexMatrix() {
    const inputElement = (i, j) => {
      const real = this.inputFloat(
        `Introduzca el valor real [${i}][${j}]: `,
        0
      );
      const imaginary = this.inputFloat(
        `Introduzca el valor imaginario [${i}][${j}]: `,
        0
      );

      console.log('\n');

      return new ComplexNumber({ real, imaginary });
    };

    return this.inputMatrix(inputElement);
  }

  static execMatrixSumCalculator() {
    console.log('Calculadora de la suma de matrices');

    const matrixA = this.inputMatrix();
    const matrixB = this.inputMatrix();

    const resultMatrix = MatrixOperator.sum(matrixA, matrixB);

    console.log('Las matrices introducidas son:');
    console.log(`${MatrixOperator.print(matrixA)}\n`);
    console.log(MatrixOperator.print(matrixB));
    console.log('\nLa matriz resultante es:');
    console.log(MatrixOperator.print(resultMatrix));
  }

  static execMatrixSubtractionCalculator() {
    console.log('Calculadora de la resta de matrices');

    const matrixA = this.inputMatrix();
    const matrixB = this.inputMatrix();

    const resultMatrix = MatrixOperator.subtract(matrixA, matrixB);

    console.log('Las matrices introducidas son:');
    console.log(`${MatrixOperator.print(matrixA)}\n`);
    console.log(MatrixOperator.print(matrixB));
    console.log('\nLa matriz resultante es:');
    console.log(MatrixOperator.print(resultMatrix));
  }

  static execMatrixMultiplicationCalculator() {
    console.log('Calculadora de la  multiplicación de matrices');

    const matrixA = this.inputMatrix();
    const matrixB = this.inputMatrix();

    const resultMatrix = MatrixOperator.multiply(matrixA, matrixB);

    console.log('Las matrices introducidas son:');
    console.log(`${MatrixOperator.print(matrixA)}\n`);
    console.log(MatrixOperator.print(matrixB));
    console.log('\nLa matriz resultante es:');
    console.log(MatrixOperator.print(resultMatrix));
  }

  static execInverseMatrixCalculator() {
    console.log('Calculadora de la matriz inversa');
    console.log('Introduzca una matriz');

    const matrix = this.inputMatrix();

    const invertedMatrix = MatrixOperator.invert(matrix);

    console.log('La matriz introducida es:');
    console.log(MatrixOperator.print(matrix));
    console.log('\nLa matriz inversa es:');
    console.log(MatrixOperator.print(invertedMatrix));
  }

  static execComplexMatrixSumCalculator() {
    console.log('Calculadora de la suma de matrices de números complejos');

    const matrixA = this.inputComplexMatrix();
    const matrixB = this.inputComplexMatrix();

    const resultMatrix = ComplexMatrixOperator.sum(matrixA, matrixB);

    console.log('Las matrices introducidas son:');
    console.log(`${ComplexMatrixOperator.print(matrixA)}\n`);
    console.log(ComplexMatrixOperator.print(matrixB));
    console.log('\nLa matriz resultante es:');
    console.log(ComplexMatrixOperator.print(resultMatrix));
  }

  static execComplexMatrixSubtractionCalculator() {
    console.log('Calculadora de la resta de matrices de números complejos');

    const matrixA = this.inputComplexMatrix();
    const matrixB = this.inputComplexMatrix();

    const resultMatrix = ComplexMatrixOperator.subtract(matrixA, matrixB);

    console.log('Las matrices introducidas son:');
    console.log(`${ComplexMatrixOperator.print(matrixA)}\n`);
    console.log(ComplexMatrixOperator.print(matrixB));
    console.log('\nLa matriz resultante es:');
    console.log(ComplexMatrixOperator.print(resultMatrix));
  }

  static execComplexMatrixMultiplicationCalculator() {
    console.log('Calculadora de la multiplicación de matrices de números complejos');

    const matrixA = this.inputComplexMatrix();
    const matrixB = this.inputComplexMatrix();

    const resultMatrix = ComplexMatrixOperator.multiply(matrixA, matrixB);

    console.log('Las matrices introducidas son:');
    console.log(`${ComplexMatrixOperator.print(matrixA)}\n`);
    console.log(ComplexMatrixOperator.print(matrixB));
    console.log('\nLa matriz resultante es:');
    console.log(ComplexMatrixOperator.print(resultMatrix));
  }

  static execInverseComplexMatrixCalculator() {
    console.log('Calculadora de la matriz de números complejos inversa');
    console.log('Introduzca una matriz');

    const matrix = this.inputComplexMatrix();

    const invertedMatrix = ComplexMatrixOperator.invert(matrix);

    console.log('La matriz introducida es:');
    console.log(ComplexMatrixOperator.print(matrix));
    console.log('\nLa matriz inversa es:');
    console.log(ComplexMatrixOperator.print(invertedMatrix));
  }
}
