import MatrixOperator from '../src/MatrixOperator.js';
import ComplexNumber from '../src/ComplexNumber.js';

export default class ComplexMatrixOperator extends MatrixOperator {
  static operate(
    matrixA,
    matrixB,
    operate = (complexA, complexB) => complexA.sum(complexB)
  ) {
    const result = this.makeNewMatrix();

    for (let i = 0; i < 3; i++)
      for (let j = 0; j < 3; j++)
        result[i][j] = operate(matrixA[i][j], matrixB[i][j]);

    return result;
  }

  static sum(matrixA, matrixB) {
    return this.operate(matrixA, matrixB);
  }

  static subtract(matrixA, matrixB) {
    return this.operate(matrixA, matrixB, (complexA, complexB) =>
      complexA.subtract(complexB)
    );
  }

  static multiply(matrixA, matrixB) {
    const init = () => new ComplexNumber({});
    const operate = (result, matrixA, matrixB, x, y, x1, y1) =>
      result[y][x].sum(matrixA[y][x1].multiply(matrixB[y1][x]));

    return super.multiply(matrixA, matrixB, init, operate);
  }

  static calculateDeterminant(matrix) {
    const determinantRow1 = matrix[0][0].multiply(
      matrix[1][1]
        .multiply(matrix[2][2])
        .subtract(matrix[2][1].multiply(matrix[1][2]))
    );
    const determinantRow2 = matrix[0][1].multiply(
      matrix[1][0]
        .multiply(matrix[2][2])
        .subtract(matrix[2][0].multiply(matrix[1][2]))
    );
    const determinantRow3 = matrix[0][2].multiply(
      matrix[1][0]
        .multiply(matrix[2][1])
        .subtract(matrix[2][0].multiply(matrix[1][1]))
    );
    const det = determinantRow1.subtract(determinantRow2).sum(determinantRow3);

    return det;
  }

  static adjugate(matrix) {
    const adjugated = [[], [], []];
    adjugated[0][0] = matrix[1][1]
      .multiply(matrix[2][2])
      .subtract(matrix[2][1].multiply(matrix[1][2]));
    adjugated[0][1] = new ComplexNumber({ real: -1, imaginary: -1 }).multiply(
      matrix[1][0]
        .multiply(matrix[2][2])
        .subtract(matrix[2][0].multiply(matrix[1][2]))
    );
    adjugated[0][2] = matrix[1][0]
      .multiply(matrix[2][1])
      .subtract(matrix[2][0].multiply(matrix[1][1]));
    adjugated[1][0] = new ComplexNumber({ real: -1, imaginary: -1 }).multiply(
      matrix[0][1]
        .multiply(matrix[2][2])
        .subtract(matrix[2][1].multiply(matrix[0][2]))
    );
    adjugated[1][1] = matrix[0][0]
      .multiply(matrix[2][2])
      .subtract(matrix[2][0].multiply(matrix[0][2]));
    adjugated[1][2] = new ComplexNumber({ real: -1, imaginary: -1 }).multiply(
      matrix[0][0]
        .multiply(matrix[2][1])
        .subtract(matrix[2][0].multiply(matrix[0][1]))
    );
    adjugated[2][0] = matrix[0][1]
      .multiply(matrix[1][2])
      .subtract(matrix[1][1].multiply(matrix[0][2]));
    adjugated[2][1] = new ComplexNumber({ real: -1, imaginary: -1 }).multiply(
      matrix[0][0]
        .multiply(matrix[1][2])
        .subtract(matrix[1][0].multiply(matrix[0][2]))
    );
    adjugated[2][2] = matrix[0][0]
      .multiply(matrix[1][1])
      .subtract(matrix[1][0].multiply(matrix[0][1]));

    return this.transpose(adjugated);
  }

  static invert(matrix) {
    const factor = new ComplexNumber({ real: 1 }).divide(
      this.calculateDeterminant(matrix)
    );
    matrix = this.adjugate(matrix);

    for (const i in matrix)
      for (const j in matrix) matrix[i][j] = matrix[i][j].multiply(factor);

    return matrix;
  }
}
