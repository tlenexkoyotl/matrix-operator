export default class ComplexNumber {
  constructor({ real = 0, imaginary = 0 }) {
    this.real = real;
    this.imaginary = imaginary;
  }

  toString() {
    if (this.real == 0 && this.imaginary == 0) return `0`;

    const real = this.real == 0 ? '' : `${this.real}`;

    const imaginary =
      this.imaginary === 0
        ? ''
        : Math.abs(this.imaginary) === 1
        ? `${
            this.imaginary > 0
              ? real.length > 0
                ? ' + '
                : ''
              : real.length > 0
              ? ' -'
              : '-'
          }i`
        : this.imaginary > 0
        ? `${real.length > 0 ? ' + ' : ''}${this.imaginary}i`
        : `${real.length > 0 ? ' ' : ''}${this.imaginary}i`;

    return `${real}${imaginary}`;
  }

  operate(
    anotherComplex,
    operate = (thisComplex, anotherComplex) =>
      new ComplexNumber({
        real: thisComplex.real + anotherComplex.real,
        imaginary: thisComplex.imaginary + anotherComplex.imaginary,
      })
  ) {
    return operate(this, anotherComplex);
  }

  sum(anotherComplex) {
    return this.operate(anotherComplex);
  }

  subtract(anotherComplex) {
    return this.operate(
      anotherComplex,
      (thisComplex, anotherComplex) =>
        new ComplexNumber({
          real: thisComplex.real - anotherComplex.real,
          imaginary: thisComplex.imaginary - anotherComplex.imaginary,
        })
    );
  }

  multiply(anotherComplex) {
    const operate = (thisComplex, anotherComplex) => {
      const real =
        thisComplex.real * anotherComplex.real -
        thisComplex.imaginary * anotherComplex.imaginary;
      const imaginary =
        thisComplex.real * anotherComplex.imaginary +
        thisComplex.imaginary * anotherComplex.real;

      return new ComplexNumber({ real, imaginary });
    };

    return this.operate(anotherComplex, operate);
  }

  getConjugate() {
    return new ComplexNumber({ real: this.real, imaginary: -this.imaginary });
  }

  divide(anotherComplex) {
    const operate = (thisComplex, anotherComplex) => {
      const conjugate = anotherComplex.getConjugate();
      const numerator = thisComplex.multiply(conjugate);
      const denominator = anotherComplex.multiply(conjugate).real;

      return new ComplexNumber({
        real: numerator.real / denominator,
        imaginary: numerator.imaginary / denominator,
      });
    };

    return this.operate(anotherComplex, operate);
  }
}
