export default class MatrixOperator {
  static makeNewMatrix() {
    return [[], [], []];
  }

  static operate(matrixA, matrixB, operate = (a, b) => a + b) {
    const result = this.makeNewMatrix();

    for (let i = 0; i < 3; i++)
      for (let j = 0; j < 3; j++)
        result[i][j] = operate(matrixA[i][j], matrixB[i][j]);

    return result;
  }

  static sum(matrixA, matrixB) {
    return this.operate(matrixA, matrixB);
  }

  static subtract(matrixA, matrixB) {
    return this.operate(matrixA, matrixB, (a, b) => a - b);
  }

  static multiply(
    matrixA,
    matrixB,
    init = () => 0,
    operate = (result, matrixA, matrixB, x, y, x1, y1) => {
      result[y][x] += matrixA[y][x1] * matrixB[y1][x];

      return result[y][x];
    }
  ) {
    const result = this.makeNewMatrix();

    for (let y = 0; y < 3; y++)
      for (let x = 0; x < 3; x++) {
        let y1 = 0;
        result[y][x] = init();

        for (let x1 = 0; x1 < 3; x1++)
          result[y][x] = operate(result, matrixA, matrixB, x, y, x1, y1++);
      }

    return result;
  }

  static calculateDeterminant(matrix) {
    const determinantRow1 =
      matrix[0][0] *
      (matrix[1][1] * matrix[2][2] - matrix[2][1] * matrix[1][2]);
    const determinantRow2 =
      matrix[0][1] *
      (matrix[1][0] * matrix[2][2] - matrix[2][0] * matrix[1][2]);
    const determinantRow3 =
      matrix[0][2] *
      (matrix[1][0] * matrix[2][1] - matrix[2][0] * matrix[1][1]);
    const det = determinantRow1 - determinantRow2 + determinantRow3;

    return det;
  }

  static transpose(matrix) {
    const transposed = this.makeNewMatrix();

    for (const j in matrix)
      for (const i in matrix) transposed[i][j] = matrix[j][i];

    return transposed;
  }

  static adjugate(matrix) {
    const adjugated = [[], [], []];
    adjugated[0][0] = matrix[1][1] * matrix[2][2] - matrix[2][1] * matrix[1][2];
    adjugated[0][1] =
      -1 * (matrix[1][0] * matrix[2][2] - matrix[2][0] * matrix[1][2]);
    adjugated[0][2] = matrix[1][0] * matrix[2][1] - matrix[2][0] * matrix[1][1];
    adjugated[1][0] =
      -1 * (matrix[0][1] * matrix[2][2] - matrix[2][1] * matrix[0][2]);
    adjugated[1][1] = matrix[0][0] * matrix[2][2] - matrix[2][0] * matrix[0][2];
    adjugated[1][2] =
      -1 * (matrix[0][0] * matrix[2][1] - matrix[2][0] * matrix[0][1]);
    adjugated[2][0] = matrix[0][1] * matrix[1][2] - matrix[1][1] * matrix[0][2];
    adjugated[2][1] =
      -1 * (matrix[0][0] * matrix[1][2] - matrix[1][0] * matrix[0][2]);
    adjugated[2][2] = matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1];

    return this.transpose(adjugated);
  }

  static invert(matrix) {
    const factor = 1 / this.calculateDeterminant(matrix);
    matrix = this.adjugate(matrix);

    for (const i in matrix) for (const j in matrix) matrix[i][j] *= factor;

    return matrix;
  }

  static print(matrix) {
    let print = '';

    for (let y = 0; y < 3; y++)
      for (let x = 0; x < 3; x++)
        print += `${y == 0 && x == 0 ? '[\n' : ''}${x == 0 ? '\t[ ' : ''}(${
          matrix[y][x]
        })${x < 2 ? ',\t' : ''}${x == 2 ? ' ],\n' : ''}${
          y == 2 && x == 2 ? ']' : ''
        }`;

    return print;
  }
}
